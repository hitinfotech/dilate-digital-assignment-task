var norris = {
    list: $("#jock_feed"),
    favourites_list_ele: $("#favourite_feed"),
    favourites:[],
    interval:5000,
    interval_value:0,
    timer:null,
    jokeList : [],
    emptyList:function(){ 
        this.list.html('');
        this.jokeList=[] 
    },
    fetchFeed : function(){
        var self = norris;
        $this = $("#referesh_jock");
        $.ajax({
            url:'http://api.icndb.com/jokes/random/10',
            type:'POST',
            dataType:'json',
            beforeSend:function(){
                $this.button("loading");
            },
            complete:function(){
                $this.button("reset");
            },
            success:function(json){
                self.emptyList();
                if(json['type'] == 'success'){
                    self.jokeList = json['value'];
                    self._updateList();
                }
            },
        })
    },
    _updateList: function(){
        var self = norris;
        self.list.html('');
        $.each(self.jokeList, function(i,joke){
            var is_fav = self._exist(joke) ? 'is-active' : '';

            var li = $('<li/>')
            .addClass('list-group-item')
            .html(joke.joke);

            var button = $("<button/>")
            .html("<i class='glyphicon glyphicon-heart'>")
            .click({joke:joke,mark:true}, self.addTofavourites)
            .addClass("favourites-button btn btn-primary btn-sm " + is_fav)
            .appendTo(li);

            li.appendTo(self.list);
        })
    },
    _exist: function(joke){
        var self = norris;
        return self.favourites.indexOf(joke) >= 0 ? true : false;
    },
    _setStorage: function(){
        var self = norris;
        localStorage.setItem("jock",  JSON.stringify(self.favourites) );
    },
    init: function(){
        var self = norris;

        var fav = localStorage.getItem("jock");
        if(fav) {
            self.favourites = JSON.parse(fav);
            self.favourite_feedUpdate();  
            self._updateList();
        }
    },
    addTofavourites : function(event){
        var self = norris;
        var joke = event.data.joke;

        if(!self._exist(joke)){
            $(".favourite_duplicate-error").fadeOut();

            if(self.favourites.length >= 10){
                $(".favourite_feed-error").fadeIn();
            } else {
                $(".favourite_feed-error").fadeOut();
                self.favourites.push(joke);
                self.favourite_feedUpdate();  
                self._updateList();
                self._setStorage();
            }
        } else {
            $(".favourite_duplicate-error").fadeIn();
        }

    },
    favourite_feedUpdate: function(){
        var self = norris;
        self.favourites_list_ele.html('');

        $.each(self.favourites, function(i,joke){
            var li = $('<li/>')
            .addClass('list-group-item')
            .html(joke.joke);

            var button = $("<button/>")
            .html("<i class='glyphicon glyphicon-trash'>")
            .click({joke:joke}, self.removeFavourites)
            .addClass("favourites-button btn btn-danger btn-sm")
            .appendTo(li);

            li.appendTo(self.favourites_list_ele);
        })
    },
    removeFavourites : function(event){
        var self = norris;
        var joke = event.data.joke;

        var index = self.favourites.indexOf(joke);
        self.favourites.splice(index,1);
        self.favourite_feedUpdate();
        self._updateList();
        self._setStorage();

        $(".favourite_feed-error").fadeOut();
    },
    timerStart: function(){
        var self = norris;
        if (self.timer !== null) return;
        self.timer = setInterval(function () {
            if(self.favourites.length >= 10){ return false }
                $.ajax({
                    url:'http://api.icndb.com/jokes/random/1',
                    type:'POST',
                    dataType:'json',
                    success:function(json){
                        if(json['type'] == 'success'){
                            $.each(json['value'], function(i,joke){
                                self.addTofavourites({ data: { joke:joke } })
                            })
                        }
                    },
                })
        }, self.interval); 
    },
    timerStop: function(){
        var self = norris;
        clearInterval(self.timer);
        self.timer = null;
    }
}

norris.init();
//norris.fetchFeed();
$('[data-toggle="tooltip"]').tooltip(); 
$("#referesh_jock").click(norris.fetchFeed);
$("input[name=auto_favourite]").change(function(){
    var status = $(this).prop("checked");
    if(status){
        norris.timerStop();
        norris.timerStart();
    } else {
        norris.timerStop();
    }
});